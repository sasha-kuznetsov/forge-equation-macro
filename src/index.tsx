import ForgeUI, {
  render,
  Fragment,
  Macro,
  Image,
  Text,
  ConfigForm,
  TextField,
  useConfig,
  useState,
} from "@forge/ui";
import EquationGenerator from "./util/generator";

const App = () => {
  const { equation } = useConfig();

  const generator = new EquationGenerator();

  const errorView = (response) => {
    return (
      <Fragment>
        <Text content={`Error ${response.error} for equation ${equation}`} />
      </Fragment>
    );
  };

  const imageView = (response) => {
    return (
      <Fragment>
        <Image
          src={`data:image/svg+xml;utf8,${encodeURIComponent(response.svg)}`}
          alt={equation}
        />
      </Fragment>
    );
  };

  const [output] = useState(
    async () =>
      await generator.generate({
        equation,
      })
  );

  return output.error ? errorView(output) : imageView(output);
};

const Config = () => {
  const equationPlaceholder = "Enter your equation in LaTeX format i.e. e=mc^2";

  return (
    <ConfigForm>
      <TextField
        name="equation"
        isRequired={true}
        placeholder={equationPlaceholder}
        label="Your Equation"
      />
    </ConfigForm>
  );
};

export const run = render(
  <Macro
    app={<App />}
    config={<Config />}
    defaultConfig={{
      equation: "e = mc^2",
    }}
  />
);
